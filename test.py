import vice
import os

def printResults(testData, compressedData, decompressedData, barSize=30):
    print("compression ratio: " + str((len(compressedData)/len(testData))*100) + "%")

    # print(testData)
    # print(compressedData)
    # print(decompressedData)

    faultCount = 0.0
    for i in range(len(decompressedData)):
        try:
            if decompressedData[i] != testData[i]:
                faultCount += 1
        except:
            pass

    print("fault count: " + str(faultCount))


comp = vice.compressor()


testSize = 3000

testSample = b"" + os.urandom(testSize)


compressedSample = comp.encode(testSample)

result = comp.decode(compressedSample)


printResults(testSample, compressedSample, result)