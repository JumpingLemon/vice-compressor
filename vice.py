import numpy
import tqdm

class compressor():
    def __init__(self) -> None:
        self.ct = None
        
        self.escape = b"\\"
        self.blockStart = b"<"
        self.blockEnd = b">"

        self.forbidden = self.escape + self.blockStart + self.blockEnd
        
    def _statusCallback(self, progress, outOf, task):
        if self.ct != task:
            self.ct = task
            print("-" + task + "-")
            
            self.tq = tqdm.tqdm(total = outOf)
        
        self.tq.update()
        
        if progress == outOf-1:
            self.tq.close()
        
    def _escape(self, data):
        ret = b""
        for i in range(len(data)):
            # self._statusCallback(i, len(data), "escaping")
            
            if data[i] in self.forbidden:
                ret += self.escape + bytes([data[i]])
            
            else:
                ret += bytes([data[i]])
                
        return ret
    
    def _unescape(self, data):
        ret = b""
        
        i = 0
        while True:
            # self._statusCallback(i, len(data), "un-escaping")
            
            if data[i] == self.escape[0]:
                ret += bytes([data[i+1]])
                i += 2
            else:
                ret += bytes([data[i]])
                i += 1 
                
            if i >= len(data):
                break
        return ret
        
        
    def encode(self, data):
        ret = self._escape(data)
        
        #find block of repeating data and minimize it
        
        return ret
    
    def decode(self, data):
        ret = b""
        
        #revert repeating data
        
        #un escaping is the last part
        ret = self._unescape(data)
        
        return ret
        